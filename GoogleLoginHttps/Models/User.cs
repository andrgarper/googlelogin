﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GoogleLoginHttps.Models
{
    public class User: GoogleLoginViewModel
    {
        public int Id { get; set; }
        public int Rol { get; set; }
        public int Wallet { get; set; }
        [InverseProperty("User")]
        List<Sale> Sales { get; set; }
    }
}
