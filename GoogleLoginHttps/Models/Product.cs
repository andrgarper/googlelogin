﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GoogleLoginHttps.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public int Price { get; set; }
        [InverseProperty("Product")]
        List<Sale> Sales { get; set; }
    }
}
