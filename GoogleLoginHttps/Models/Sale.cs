﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoogleLoginHttps.Models
{
    public class Sale
    {

        public int Id { get; set; }

        public int Product_Id { get; set; }
        public int User_Id { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime BookingDate { get; set; }
        public int PrecioCompra { get; set; }


        [ForeignKey("Product_Id")]
        [ValidateNever]
        public Product Product { get; set; }

        [ForeignKey("User_Id")]
        [ValidateNever]
        public User User { get; set; }
    }
}
