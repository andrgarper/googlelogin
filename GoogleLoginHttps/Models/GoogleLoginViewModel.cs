﻿using System.Security.Claims;

namespace GoogleLoginHttps.Models
{
    public class GoogleLoginViewModel
    {
        public string IdGoogle { get; set; }
        public string Name { get; set; }
        public string GiveName { get; set; }
        public string Surname { get; set; }
        public string Emailaddress { get; set; }
        public string Image { get; set; }

        public GoogleLoginViewModel(string id,string name,string givenName,string surname,string emailaddress,string image)
        {
            IdGoogle = id;
            Name = name;
            GiveName = givenName;
            Surname = surname;
            Emailaddress = emailaddress;
            Image = image;
        }

        public GoogleLoginViewModel()
        {
            
        }
    }
}
