﻿using GoogleLoginHttps.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Security.Claims;
using GoogleLoginHttps.Data;
using Microsoft.EntityFrameworkCore;

namespace GoogleLoginHttps.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly TiendicaContext _context;

        public HomeController(ILogger<HomeController> logger, TiendicaContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View();
            }
            return RedirectToAction("Logged");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                await HttpContext.SignOutAsync();
            }
            HttpContext.ChallengeAsync(GoogleDefaults.AuthenticationScheme, new AuthenticationProperties() { RedirectUri = Url.Action("GoogleResponse") });
        }

        public async Task<IActionResult> GoogleResponse()
        {
            var claimsPrincipal = HttpContext.User.Identity as ClaimsIdentity;
            var result = await HttpContext.AuthenticateAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            var listaRes = result.Principal.Identities.FirstOrDefault().Claims.Select(e => e.Value).ToList();
            await CheckAndRegisterAsync(listaRes);
            return RedirectToAction("Logged");
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Logged()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }
            var user = await GetGoogle();
            ViewBag.User = user;
            var userSales = _context.Sale.Where(e => e.User_Id == user.Id);
            ViewBag.TotalSales = userSales.ToList().Count;
            ViewBag.TotalMoneySpend = userSales.ToList().Sum(e => e.PrecioCompra);
            return View();
        }

        public async Task<IActionResult> AddMoney()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }

            var user = await GetGoogle();
            user.Wallet += 5;
            _context.Users.Update(user);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }


        private async Task<User> GetGoogle()
        {
            var valores = HttpContext.User.Claims.Select(e => e.Value).ToList();
            var google = valores.Count == 5 ? new GoogleLoginViewModel(valores[0], valores[1], valores[2], " ", valores[3], valores[4]) : new GoogleLoginViewModel(valores[0], valores[1], valores[2], valores[3], valores[4], valores[5]);
            var user = await _context.Users.FirstAsync(e => e.IdGoogle == google.IdGoogle && e.Emailaddress == google.Emailaddress);

            if (user != null)
                return user;

            return null;
        }

        private async Task CheckAndRegisterAsync(List<string> lista)
        {
            if(lista.Count == 5 && _context.Users.FirstOrDefault(e => e.IdGoogle == lista[0] && e.Emailaddress == lista[3]) == null || 
               lista.Count == 6 && _context.Users.FirstOrDefault(e => e.IdGoogle == lista[0] && e.Emailaddress == lista[4]) == null)
            {
                if (lista.Count == 5)
                    _context.Add(new User{ IdGoogle = lista[0], Name = lista[1], GiveName = lista[2], Surname = " ", Emailaddress = lista[3], Image = lista[4], Rol = 0 });
                else
                    _context.Add(new User { IdGoogle = lista[0], Name = lista[1], GiveName = lista[2], Surname = lista[3], Emailaddress = lista[4], Image = lista[5], Rol = 0});

                await _context.SaveChangesAsync();
            }
        }
    }
}