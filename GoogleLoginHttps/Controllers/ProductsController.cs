﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GoogleLoginHttps.Data;
using GoogleLoginHttps.Models;

namespace GoogleLoginHttps.Controllers
{
    public class ProductsController : Controller
    {
        private readonly TiendicaContext _context;

        public ProductsController(TiendicaContext context)
        {
            _context = context;
        }

        // GET: Products
        public async Task<IActionResult> Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            ViewBag.User = await GetGoogle();

            return View(await _context.Products.ToListAsync());
        }

        // GET: Products/Create
        public async Task<IActionResult> Create()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            ViewBag.User = await GetGoogle();

            if (ViewBag.User.Rol == 0)
                return RedirectToAction("Index", "Home");

            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Url,Price")] Product product)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            ViewBag.User = await GetGoogle();

            if (ViewBag.User.Rol == 0)
                return RedirectToAction("Index", "Home");

            if (ModelState.IsValid)
            {
                _context.Add(product);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            ViewBag.User = await GetGoogle();

            if (ViewBag.User.Rol == 0)
                return RedirectToAction("Index", "Home");

            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Url,Price")] Product product)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            ViewBag.User = await GetGoogle();

            if (ViewBag.User.Rol == 0)
                return RedirectToAction("Index", "Home");

            if (id != product.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            ViewBag.User = await GetGoogle();

            if (ViewBag.User.Rol == 0)
                return RedirectToAction("Index", "Home");

            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            ViewBag.User = await GetGoogle();

            if (ViewBag.User.Rol == 0)
                return RedirectToAction("Index", "Home");

            if (_context.Products == null)
            {
                return Problem("Entity set 'TiendicaContext.Products'  is null.");
            }
            var product = await _context.Products.FindAsync(id);
            if (product != null)
            {
                _context.Products.Remove(product);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
          return _context.Products.Any(e => e.Id == id);
        }
        public async Task<IActionResult> ComprarAsync(int id)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            var user = await GetGoogle();

            if (id == null || _context.Products == null)
            {
                return NotFound();
            }
            var product = await _context.Products.FindAsync(id);
            
            if (user != null && product != null)
            {
                if(user.Wallet >= product.Price || user.Rol == 1)
                {
                    _context.Add(new Sale { Product_Id = product.Id, User_Id = user.Id, BookingDate = DateTime.Now, PrecioCompra = product.Price });
                    user.Wallet = user.Wallet - product.Price;
                    _context.Users.Update(user);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index", "Sales");
                }
            }

            ViewBag.Moroso = true;
            ViewBag.User = user;
            return View("Index",await _context.Products.ToListAsync());

        }

        private async Task<User> GetGoogle()
        {
            var valores = HttpContext.User.Claims.Select(e => e.Value).ToList();
            var google = valores.Count == 5 ? new GoogleLoginViewModel(valores[0], valores[1], valores[2], " ", valores[3], valores[4]) : new GoogleLoginViewModel(valores[0], valores[1], valores[2], valores[3], valores[4], valores[5]);
            var user = await _context.Users.FirstAsync(e => e.IdGoogle == google.IdGoogle && e.Emailaddress == google.Emailaddress);
            
            if(user!=null)
            return user;

            return null;
        }

    }
}
