﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GoogleLoginHttps.Data;
using GoogleLoginHttps.Models;

namespace GoogleLoginHttps.Controllers
{
    public class UsersController : Controller
    {
        private readonly TiendicaContext _context;

        public UsersController(TiendicaContext context)
        {
            _context = context;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            ViewBag.User = await GetGoogle();

            if (ViewBag.User.Rol == 0)
                return RedirectToAction("Index", "Home");

            return View(await _context.Users.ToListAsync());
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            ViewBag.User = await GetGoogle();

            if (ViewBag.User.Rol == 0)
                return RedirectToAction("Index", "Home");

            if (id == null || _context.Users == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            ViewBag.User = await GetGoogle();

            if (ViewBag.User.Rol == 0)
                return RedirectToAction("Index", "Home");

            if (_context.Users == null)
            {
                return Problem("Entity set 'TiendicaContext.Users'  is null.");
            }
            var user = await _context.Users.FindAsync(id);
            if (user != null)
            {
                _context.Users.Remove(user);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
        private async Task<User> GetGoogle()
        {
            var valores = HttpContext.User.Claims.Select(e => e.Value).ToList();
            var google = valores.Count == 5 ? new GoogleLoginViewModel(valores[0], valores[1], valores[2], " ", valores[3], valores[4]) : new GoogleLoginViewModel(valores[0], valores[1], valores[2], valores[3], valores[4], valores[5]);
            var user = await _context.Users.FirstAsync(e => e.IdGoogle == google.IdGoogle && e.Emailaddress == google.Emailaddress);

            if (user != null)
                return user;

            return null;
        }
    }
}
