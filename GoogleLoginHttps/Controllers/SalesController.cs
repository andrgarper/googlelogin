﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GoogleLoginHttps.Data;
using GoogleLoginHttps.Models;

namespace GoogleLoginHttps.Controllers
{
    public class SalesController : Controller
    {
        private readonly TiendicaContext _context;

        public SalesController(TiendicaContext context)
        {
            _context = context;
        }

        // GET: Sales
        public async Task<IActionResult> Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            ViewBag.User = await GetGoogle();
            int id = ViewBag.User.Id;

            var tiendicaContext = _context.Sale.Where(s => s.User_Id == id).OrderByDescending(i => i.BookingDate).Include(s => s.Product).Include(s => s.User);

            if (ViewBag.User.Rol == 1)
                tiendicaContext = _context.Sale.OrderByDescending(i => i.BookingDate).Include(s => s.Product).Include(s => s.User);

            return View(await tiendicaContext.ToListAsync());
        }

        // GET: Sales/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            ViewBag.User = await GetGoogle();

            if (id == null || _context.Sale == null)
            {
                return NotFound();
            }

            var product = await _context.Sale
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Sales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            ViewBag.User = await GetGoogle();

            if (_context.Sale == null)
            {
                return Problem("Entity set 'TiendicaContext.Products'  is null.");
            }
            var sale = await _context.Sale.FindAsync(id);
            if (sale != null)
            {
                _context.Sale.Remove(sale);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SaleExists(int id)
        {
          return _context.Sale.Any(e => e.Id == id);
        }

        private async Task<User> GetGoogle()
        {
            var valores = HttpContext.User.Claims.Select(e => e.Value).ToList();
            var google = valores.Count == 5 ? new GoogleLoginViewModel(valores[0], valores[1], valores[2], " ", valores[3], valores[4]) : new GoogleLoginViewModel(valores[0], valores[1], valores[2], valores[3], valores[4], valores[5]);
            var user = await _context.Users.FirstAsync(e => e.IdGoogle == google.IdGoogle && e.Emailaddress == google.Emailaddress);

            if (user != null)
                return user;

            return null;
        }
    }
}
