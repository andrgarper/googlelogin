﻿using GoogleLoginHttps.Models;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;

namespace GoogleLoginHttps.Data
{
    public class TiendicaContext : DbContext
    {
        public TiendicaContext(DbContextOptions<TiendicaContext> options) : base(options)
        {

        }
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Sale> Sale { get; set; }
    }
}
