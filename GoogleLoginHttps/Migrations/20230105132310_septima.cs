﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GoogleLoginHttps.Migrations
{
    public partial class septima : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PrecioCompra",
                table: "Sale",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PrecioCompra",
                table: "Sale");
        }
    }
}
